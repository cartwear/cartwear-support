====================
Django Cartwear Support
====================

.. image:: https://secure.travis-ci.org/tangentlabs/django-cartwear-support.png?branch=master
    :target: http://travis-ci.org/#!/tangentlabs/django-cartwear-support

.. image:: https://coveralls.io/repos/tangentlabs/django-cartwear-support/badge.png?branch=master
    :alt: Coverage
    :target: https://coveralls.io/r/tangentlabs/django-cartwear-support

**Disclaimer:** the project is still under heavy development. Things might
change rapidly, so please use with caution.

**Requires Django 1.5+ and Cartwear 0.6+ with support for custom user models**

Django Cartwear ticketing is a ticketing and support system for Cartwear. It
currently provides an interface to the customer to submit a support ticket. In
the dashboard, authorised users can see the tickets and respond or act on them.

Currently it only allow very basic functionality such as:

#. Setting the status of a ticket.
#. Reply to a customer with a message.
#. Make a note on the ticket for internal use.
#. Assign tickets to a staff user.

Features currently in the making:

#. Relating a ticket to products, orders or order lines
#. Allow file attachments
#. Integrate with Cartwear's alert system to notify the user of new replies.
#. Add templating for messages in the dashboard for quicker replies.

Longer-term direction:

* Add support for a rules engine to handle ticket-related tasks. This will
  include adding custom rules and actions that can be used globally or only
  by the support agent creating the rule/action.
* Provide an extensive templating system that can be used within ticket
  messages to respond quicker.
* Integrating an optional Service Level Agreement (SLA) workflow that
  defines time frames for ticket resolution of different types. The tickets
  are then prioritized or re-assigned according to actions related to these
  SLAs.


Screenshots
-----------

.. image:: https://github.com/tangentlabs/django-cartwear-support/raw/master/docs/source/_static/screenshots/customer_create_ticket.thumb.png
    :target: https://github.com/tangentlabs/django-cartwear-support/raw/master/docs/source/_static/screenshots/customer_create_ticket.png

.. image:: https://github.com/tangentlabs/django-cartwear-support/raw/master/docs/source/_static/screenshots/customer_ticket_list.thumb.png
    :target: https://github.com/tangentlabs/django-cartwear-support/raw/master/docs/source/_static/screenshots/customer_ticket_list.png

.. image:: https://github.com/tangentlabs/django-cartwear-support/raw/master/docs/source/_static/screenshots/dashboard_new_ticket.thumb.png
    :target: https://github.com/tangentlabs/django-cartwear-support/raw/master/docs/source/_static/screenshots/dashboard_new_ticket.png

.. image:: https://github.com/tangentlabs/django-cartwear-support/raw/master/docs/source/_static/screenshots/dashboard_update_ticket.thumb.png
    :target: https://github.com/tangentlabs/django-cartwear-support/raw/master/docs/source/_static/screenshots/dashboard_update_ticket.png


Documentation
-------------

You'll find installation instruction and further documentation at
`django-cartwear-support.rtfd.org`_ generously hosted by `readthedocs.org`_.


License
-------

*django-cartwear-support* is released under the permissive `New BSD License`_

.. _`New BSD License`: https://github.com/tangentlabs/django-cartwear-support/blob/master/LICENSE
.. _`django-cartwear-support.rtfd.org`: http://django-cartwear-support.rtfd.org
.. _`readthedocs.org`: https://readthedocs.org/
