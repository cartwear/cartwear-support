from setuptools import setup, find_packages

__version__ = "0.0.29"

setup(
    name='cartwear-support',
    version=__version__,
    url='https://github.com/ninjaas/cartwear-support',
    author="Ray Ch",
    author_email="ray@ninjaas.com",
    description="Ticketing and customer support for Cartwear",
    long_description=open('README.rst').read(),
    keywords="django, cartwear, e-commerce, customer support, issue tracking",
    license='BSD',
    packages=find_packages(exclude=['ez_setup', 'examples', '*tests', '*fixtures', 'sandbox']),
    # for avoiding conflict have one namespace for all apc related eggs.
    namespace_packages=[],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'django-shortuuidfield',
        'Django>=1.4',
        'django-cartwear',
        'django-extensions',
        'djangorestframework',
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Unix',
        'Programming Language :: Python'
    ]
)
