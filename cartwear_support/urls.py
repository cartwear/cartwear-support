from django.conf.urls import patterns, url, include

from .api import urls as api_urls
from .app import application as cartwear_support_app
from .dashboard.app import application as cartwear_support_dashboard_app


urlpatterns = patterns(
    '',
    url(r'^dashboard/support/', include(cartwear_support_dashboard_app.urls)),
    url(r'^', include(cartwear_support_app.urls)),
    url(r'^api/', include(api_urls)),
)
