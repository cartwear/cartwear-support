from django.contrib import admin
from .models import *


admin.site.register(TicketType)
admin.site.register(TicketStatus)
admin.site.register(Ticket)
admin.site.register(Priority)
admin.site.register(Message)
admin.site.register(RelatedOrder)
admin.site.register(RelatedOrderLine)
admin.site.register(RelatedProduct)
admin.site.register(Attachment)
